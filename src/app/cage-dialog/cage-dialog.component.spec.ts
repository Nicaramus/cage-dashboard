import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CageDialogComponent } from './cage-dialog.component';

describe('CageDialogComponent', () => {
  let component: CageDialogComponent;
  let fixture: ComponentFixture<CageDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CageDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
