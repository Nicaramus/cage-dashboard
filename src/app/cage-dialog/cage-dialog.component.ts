import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Cage, CageStyleEnum} from '../shared/models/cage';

@Component({
  selector: 'app-cage-dialog',
  templateUrl: './cage-dialog.component.html',
  styleUrls: ['./cage-dialog.component.css']
})
export class CageDialogComponent implements OnInit {
  cageStyles = CageStyleEnum;
  height: number = 2;
  width: number = 2;
  cage : Cage;

  constructor(public dialogRef: MatDialogRef<CageDialogComponent>) {
    this.cage = new Cage();
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getCage() {
    return this.cage;
  }

}
