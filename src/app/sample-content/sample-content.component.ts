import {Component, OnInit} from '@angular/core';
import {DynamicWidgetComponent} from '../dynamic-widget-component';

@Component({
  selector: 'app-sample-content',
  templateUrl: './sample-content.component.html',
  styleUrls: ['./sample-content.component.css']
})
export class SampleContentComponent extends DynamicWidgetComponent implements OnInit {

  ngOnInit() {
  }

}
