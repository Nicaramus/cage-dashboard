export type CageStyleEnum = '' | 'c/' | 'g/' | 'gif/';

export const CageStyleEnum = {
  CALM: '' as CageStyleEnum,
  CRAZY: 'c/' as CageStyleEnum,
  GRAY: 'g/' as CageStyleEnum,
  GIF: 'gif/' as CageStyleEnum
};

export class Cage {
  private _width: number;
  private _height: number;
  private _style: CageStyleEnum;
  private _url: string;

  constructor(width = 200, height = 200, style = CageStyleEnum.CALM) {
    this._width = width;
    this._height = height;
    this._style = style;
    this._url = 'http://www.placecage.com/' + style + width + '/' + height;
  }

  get width(): number {
    return this._width;
  }

  set width(width: number) {
    this._width = width;
    this._url = 'http://www.placecage.com/' + this._style + this._width + '/' + this._height;
  }

  get height(): number {
    return this._height;
  }

  set height(height: number) {
    this._height = height;
    this._url = 'http://www.placecage.com/' + this._style + this._width + '/' + this._height;
  }

  get style(): CageStyleEnum {
    return this._style;
  }

  set style(style: CageStyleEnum) {
    this._style = style;
    this._url = 'http://www.placecage.com/' + this._style + this._width + '/' + this._height;
  }

  get url(): string {
    return this._url;
  }
}
