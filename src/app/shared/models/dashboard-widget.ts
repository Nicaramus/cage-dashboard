export class DashboardWidget {
  constructor(x=undefined, y=undefined, width=undefined, height=undefined, type=undefined, data=undefined, options=undefined) {
    this.x = x;
    this.y = y;
    this.w = width;
    this.h = height;
    this.type = type;
    this.data = data;
  }

  x: number;
  y: number;
  w: number;
  h: number;
  type: string;
  data: any;
}
