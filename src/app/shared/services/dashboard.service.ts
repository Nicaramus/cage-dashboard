import {DashboardWidget} from '../models/dashboard-widget';
import {CageImageComponent} from '../../cage-image/cage-image.component';
import {UnknownContentComponent} from '../../unknown-content/unknown-content.component';
import {SampleContentComponent} from '../../sample-content/sample-content.component';

export class DashboardService {

  widgets: DashboardWidget[] = [];

  private mappings = {
    'cage': CageImageComponent,
    'sample': SampleContentComponent
  };

  getComponentType(typeName: string) {
    let type = this.mappings[typeName];
    return type || UnknownContentComponent;
  }

  constructor() {
    // this.widgets = [
    //   new DashboardWidget(0, 0, 3, 1),
    //   new DashboardWidget(4, 0, 2, 2)
    // ];
  }

  addWidget(widget: DashboardWidget) {
    this.widgets.push(widget);
  }

  addWidgetByDrop(event: any, type: string) {
    this.widgets.push(new DashboardWidget(
      event.item.x,
      event.item.y,
      event.item.w,
      event.item.h,
      type
    ));
  }

  getWidgets() {
    return this.widgets;
  }

  p
}
