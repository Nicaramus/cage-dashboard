import {Component, ComponentFactoryResolver, ComponentRef, Input, ViewChild, ViewContainerRef} from '@angular/core';
import {DashboardService} from '../shared/services/dashboard.service';
import {DynamicWidgetComponent} from '../dynamic-widget-component';

@Component({
  selector: 'app-dashboard-widget-content',
  templateUrl: './dashboard-widget-content.component.html',
  styleUrls: ['./dashboard-widget-content.component.css']
})
export class DashboardWidgetContentComponent {
  @ViewChild('container', {read: ViewContainerRef})
  container: ViewContainerRef;

  @Input()
  type: string;

  @Input()
  data: any;

  private componentRef: ComponentRef<{}>;

  constructor(private dashboardService: DashboardService, private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit() {
    let componentType = this.dashboardService.getComponentType(this.type);
    // note: componentType must be declared within module.entryComponents
    let factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    this.componentRef = this.container. createComponent(factory);

    let instance = <DynamicWidgetComponent> this.componentRef.instance;
    instance.data = this.data;
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }
}
