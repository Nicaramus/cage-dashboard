import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardWidgetContentComponent } from './dashboard-widget-content.component';

describe('DashboardWidgetContentComponent', () => {
  let component: DashboardWidgetContentComponent;
  let fixture: ComponentFixture<DashboardWidgetContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWidgetContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWidgetContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
