import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnknownContentComponent } from './unknown-content.component';

describe('UnknownContentComponent', () => {
  let component: UnknownContentComponent;
  let fixture: ComponentFixture<UnknownContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnknownContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnknownContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
