import { Component, OnInit } from '@angular/core';
import {DynamicWidgetComponent} from '../dynamic-widget-component';

@Component({
  selector: 'app-unknown-content',
  templateUrl: './unknown-content.component.html',
  styleUrls: ['./unknown-content.component.css']
})
export class UnknownContentComponent extends DynamicWidgetComponent implements OnInit {
  ngOnInit() {
  }
}
