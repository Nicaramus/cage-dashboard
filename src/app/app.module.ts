import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {GridsterModule} from 'angular2gridster';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {routes} from './app.routes';
import {RouterModule} from '@angular/router';
import {DashboardService} from './shared/services/dashboard.service';
import {FormsModule} from '@angular/forms';
import {MatDialogModule, MatIconModule} from '@angular/material';
import { CageImageComponent } from './cage-image/cage-image.component';
import { CageDialogComponent } from './cage-dialog/cage-dialog.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EnumPipe } from './enum.pipe';
import { UnknownContentComponent } from './unknown-content/unknown-content.component';
import { SampleContentComponent } from './sample-content/sample-content.component';
import {DashboardWidgetContentComponent} from './dashboard-widget-content/dashboard-widget-content.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    CageImageComponent,
    CageDialogComponent,
    EnumPipe,
    UnknownContentComponent,
    SampleContentComponent,
    DashboardWidgetContentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {useHash: true}),
    GridsterModule.forRoot(),
    FormsModule,
    MatIconModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  providers: [DashboardService],
  bootstrap: [AppComponent],
  entryComponents: [CageDialogComponent, CageImageComponent, UnknownContentComponent, SampleContentComponent]
})
export class AppModule { }
