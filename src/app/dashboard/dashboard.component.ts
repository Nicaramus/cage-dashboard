import {Component, OnInit, ViewChild} from '@angular/core';
import {DashboardService} from '../shared/services/dashboard.service';
import {GridsterComponent, IGridsterOptions} from 'angular2gridster';
import {DashboardWidget} from '../shared/models/dashboard-widget';
import {MatDialog} from '@angular/material';
import {CageDialogComponent} from '../cage-dialog/cage-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild(GridsterComponent) gridster: GridsterComponent;

  editMode: boolean = false;
  floating: boolean = false;

  editModeLines = {visible: true, always: true, color: '#d8d8d8', width: 1, backgroundColor: 'transparent'};
  viewModeLines = {visible: false, always: false, color: '#d8d8d8', width: 1, backgroundColor: 'transparent'};

  gridsterOptions: IGridsterOptions = {
    lanes: 7,
    lines: this.viewModeLines,
    floating: false,
    shrink: true,
    direction: 'vertical',
    dragAndDrop: false,
    resizable: false,
    useCSSTransforms: true,
    responsiveView: true
  };

  constructor(public dashboardService: DashboardService, private dialog: MatDialog) {
  }

  ngOnInit() {
  }

  addWidgetWithoutData() {
    this.dashboardService.addWidget(new DashboardWidget(undefined, undefined, 2));
    this.gridster.reload();
  }

  toggleEdit() {
    if (this.editMode) {
      this.gridster.setOption('resizable', true);
      this.gridster.setOption('dragAndDrop', true);
      this.gridster.setOption('lines', this.editModeLines);
    } else {
      this.gridster.setOption('resizable', false);
      this.gridster.setOption('dragAndDrop', false);
      this.gridster.setOption('lines', this.viewModeLines);
    }
    this.gridster.reload();
  }

  toggleFloating() {
    this.gridster.setOption('floating', this.floating);
    this.gridster.reload();
  }

  private openDialog(): void {
    let dialogRef = this.dialog.open(CageDialogComponent, {
      width: '40rem',
      panelClass: 'cage-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        this.dashboardService.addWidget(new DashboardWidget(undefined, undefined, result.width, result.height, 'cage', result.data));
      }

    });
  }

}
