import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CageImageComponent } from './cage-image.component';

describe('CageImageComponent', () => {
  let component: CageImageComponent;
  let fixture: ComponentFixture<CageImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CageImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CageImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
