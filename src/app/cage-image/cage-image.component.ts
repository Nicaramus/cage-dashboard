import {Component, Input} from '@angular/core';

import {DynamicWidgetComponent} from '../dynamic-widget-component';

@Component({
  selector: 'app-cage-image',
  templateUrl: './cage-image.component.html',
  styleUrls: ['./cage-image.component.css']
})
export class CageImageComponent extends DynamicWidgetComponent {
}
